# make_frames

## Animation processing command line tool

**make_frames** is a very simple command line tool written in **Rust** that allows to take a gif file or group of png files as an input and outputs a single png raw animation file with each frame next to each other.

## Usage:
    `make_frames [OPTIONS] <mode> <input-file>`

### Flags:
    -h, --help       Prints help information
    -V, --version    Prints version information

### Options:
    -o, --output <output-file>    

### Args:
    <mode>          
    <input-file>  

## Examples

From a gif file, automatic output destination

```
$ make_frames gif path/to/file.gif
output -> Animation frames exported to file at path/to/animation.png
```

From gif, specifying output destination

```
$ make_frames gif path.to/file.gif -o path/to/destination/file.png
output -> Animation frames exported to file at path/to/destination/file.png
```

From png folder

_In png_folder mode the input path must be a directory and must only contain .png files. Files will be ordered by name._

```
$ make_frames png_folder path/to/directory
output -> Animation frames exported to file at path/to/directory/animation.png
```

or

```
$ make_frames png_folder path/to/directory -o path/to/destination/file.png
output -> Animation frames exported to file at ath/to/destination/file.png
```

## Installation
Release package is available for Linux/Debian amd64 architectures.

Download the package : 

- <a href="https://gitlab.com/peter_rabbit/make_frames/-/blob/master/releases/deb/make_frames_0.1.1_amd64.deb" download>make_frames_0.1.1_amd64.deb</a>

Install it with
```
$ sudo apt-get install path/to/the/downloaded/package/make_frames_0.1.0_amd64.deb -y
```

