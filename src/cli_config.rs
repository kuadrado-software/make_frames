//! The cli_config module validates the command line input data and returns a CliConfig Structure
use std::collections::HashMap;
use std::ffi::OsStr;
use std::path::{Path, PathBuf};
/// The two supported input modes in make_frames
#[derive(Debug, PartialEq, Eq)]
pub enum FrameSourceMode {
    PngFolder,
    Gif,
}

/// The returned structured for the command configuration
#[derive(Debug)]
pub struct CliConfig<'a> {
    pub mode: &'a FrameSourceMode,
    pub input_file: PathBuf,
    pub output_file: PathBuf,
}

/// get_mode returns one of the FrameSourceMode enum options using an HashMap and the input string from the command line
/// as the key. Return an error if specified mode is invalid.
fn get_mode(s: &String) -> Result<&'static FrameSourceMode, &'static str> {
    let mut mode_map = HashMap::new();
    mode_map.insert(String::from("gif"), &FrameSourceMode::Gif);
    mode_map.insert(String::from("png_folder"), &FrameSourceMode::PngFolder);

    match mode_map.get(s) {
        Some(mode) => Ok(*mode),
        None => Err("Invalid image sequence type, must be gif or png_folder"),
    }
}

fn valid_input_gif(input_path: &Path) -> Result<(), &'static str> {
    let ext = match input_path.extension() {
        Some(ftype) => ftype,
        None => OsStr::new(""),
    };

    if ext != "gif" {
        return Err("Gif image sequence mode were specified but input path is not a .gif file");
    }

    return Ok(());
}

fn valid_input_png_folder(input_path: &Path) -> Result<(), &'static str> {
    let metadata = input_path.metadata().unwrap();
    if !metadata.is_dir() {
        return Err("Input path should be a directory for png_folder mode");
    }
    let dir_contents: Vec<std::fs::DirEntry> =
        input_path.read_dir().unwrap().map(|f| f.unwrap()).collect();

    for file in dir_contents {
        let fpath = file.path();
        let meta = file.metadata().unwrap();
        let isfile = meta.is_file();
        if !isfile {
            return Err(
                "Input directory should only contain png files but contains a subdirectory",
            );
        }
        let ext = match fpath.extension() {
            Some(ftype) => ftype,
            None => OsStr::new(""),
        };
        let ispng = ext == "png";
        if !ispng {
            return Err("Input directory should only contain png files");
        }
    }
    return Ok(());
}

use crate::cli_args::CliArgs;
impl<'a> CliConfig<'a> {
    /// Creates a new CliConfig Result and performs a validation on command line input.
    /// Returns an error if Gif mode is specified but input is not a gif file, or
    /// if png_folder mode is specified but input is not a diretory or doesn't contain only png files.
    pub fn new(args: CliArgs) -> Result<CliConfig<'a>, &'static str> {
        let mode = match get_mode(&args.mode) {
            Ok(mode) => mode,
            Err(error) => return Err(error),
        };

        if !args.input_file.exists() {
            return Err("Specified source path does not exist");
        }

        let input_file = {
            match mode {
                &FrameSourceMode::Gif => match valid_input_gif(&args.input_file) {
                    Ok(()) => (),
                    Err(err) => return Err(err),
                },
                &FrameSourceMode::PngFolder => match valid_input_png_folder(&args.input_file) {
                    Ok(()) => (),
                    Err(err) => return Err(err),
                },
            }

            args.input_file
        };

        let output_file = match args.output_file {
            Some(path) => {
                if !path.parent().unwrap().exists() {
                    return Err("Output file destination does not exist.")
                }
                path
            },
            None => {
                let parent: PathBuf = match mode {
                    FrameSourceMode::Gif => input_file.parent().unwrap().to_path_buf(),
                    FrameSourceMode::PngFolder => input_file.clone(),
                };
                parent.join("animation.png")
            }
        };

        Ok(CliConfig {
            mode,
            input_file,
            output_file,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_PATH_GIF: &str = "resources/testing/gif/heart_pix.gif";
    static INPUT_PATH_FOLDER: &str = "resources/testing/pngs";
    static OUTPUT_PATH: &str = "output/path";
    static MODE_PNG_FOLDER: &str = "png_folder";
    static INVALID_MODE: &str = "aa";
    static MODE_GIF: &str = "gif";

    #[test]
    fn valid_gif_config() {
        let conf_1 = CliConfig::new(vec![String::from(MODE_GIF), String::from(INPUT_PATH_GIF)]);
        let conf_2 = CliConfig::new(vec![
            String::from(MODE_GIF),
            String::from(INPUT_PATH_GIF),
            String::from(OUTPUT_PATH),
        ]);
        assert!(conf_1.is_ok() && conf_2.is_ok());
    }

    #[test]
    fn invalid_gif_config() {
        let conf_1 = CliConfig::new(vec![
            String::from(MODE_GIF),
            String::from(INPUT_PATH_FOLDER),
        ]);
        let conf_2 = CliConfig::new(vec![
            String::from(MODE_GIF),
            String::from(INPUT_PATH_FOLDER),
            String::from(OUTPUT_PATH),
        ]);
        let conf_3 = CliConfig::new(vec![
            String::from(INVALID_MODE),
            String::from(INPUT_PATH_GIF),
        ]);

        assert!(conf_1.is_err() && conf_2.is_err() && conf_3.is_err());
    }

    #[test]
    fn valid_png_folder_config() {
        let conf_1 = CliConfig::new(vec![
            String::from(MODE_PNG_FOLDER),
            String::from(INPUT_PATH_FOLDER),
        ]);
        let conf_2 = CliConfig::new(vec![
            String::from(MODE_PNG_FOLDER),
            String::from(INPUT_PATH_FOLDER),
            String::from(OUTPUT_PATH),
        ]);
        assert!(conf_1.is_ok() && conf_2.is_ok());
    }

    #[test]
    fn invalid_png_folder_config() {
        let conf_1 = CliConfig::new(vec![
            String::from(MODE_PNG_FOLDER),
            String::from(INPUT_PATH_GIF),
        ]);
        assert!(conf_1.is_err());
    }
}
