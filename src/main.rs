//! **make_frames** is a very simple command line tool that allow to take a gif or group of png files
//! as an input and output a single png raw animation file with each frame next to each other.

mod cli_config;
mod frames;
mod gif_to_frames;
mod png_folder_to_frames;

use cli_config::{CliConfig, FrameSourceMode};
use std::process;

mod cli_args;
use cli_args::CliArgs;
use structopt::StructOpt;

fn main() {
    let args = CliArgs::from_args();
    let config = CliConfig::new(args).unwrap_or_else(|error| {
        eprintln!("Error parsing command arguments: {}", error);
        process::exit(1);
    });

    match config.mode {
        FrameSourceMode::Gif => {
            gif_to_frames::build(&config.input_file, &config.output_file);
        }
        FrameSourceMode::PngFolder => {
            png_folder_to_frames::build(&config.input_file, &config.output_file);
        }
    }
}
