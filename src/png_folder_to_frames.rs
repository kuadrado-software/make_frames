use crate::frames::build_png_output;
use image;
use std::path::PathBuf;

pub fn build(input_path: &PathBuf, output_path: &PathBuf) {
    // Read dir
    let mut dir_contents: Vec<std::fs::DirEntry> =
        input_path.read_dir().unwrap().map(|f| f.unwrap()).collect();

    // Sort files by name
    dir_contents.sort_by(|a, b| a.file_name().cmp(&b.file_name()));

    let mut frame_width = 0;
    let mut frame_height = 0;
    let frames_count = dir_contents.len();

    let mut frames = vec![];

    for file in dir_contents {
        let img = image::open(file.path()).unwrap().into_rgba8();
        let dim = img.dimensions();
        if frame_width == 0 && frame_height == 0 {
            frame_width = dim.0;
            frame_height = dim.1;
        } else if dim.0 != frame_width || dim.1 != frame_height {
            panic!("Frames must be all the same size");
        }

        frames.push(img.into_raw());
    }

    let result_img_buf = build_png_output(frames, frame_width, frame_height, frames_count);

    // Create the output file
    result_img_buf.save(output_path).unwrap();

    println!(
        "{}",
        String::from("Animation frames exported to file at ") + output_path.to_str().unwrap()
    );
}
