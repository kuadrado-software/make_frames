//! The frames module contains convenient data structures to hold the input image data.
use image;

/// The RgbaPix struct is meant to store an rgba pixel color data and its position into the final output image buffer
pub struct RgbaPix {
    pub color: Vec<u8>,
    pub position: Vec<u32>,
}

/// The RgbaFrames Buffer struct stores a collection of pixels: RgbaPix
pub struct RgbaFramesBuffer {
    pub frames: Vec<RgbaPix>,
}

pub fn build_png_output(
    raw_frames: Vec<Vec<u8>>,
    frame_width: u32,
    frame_height: u32,
    frames_count: usize,
) -> image::ImageBuffer<image::Rgba<u8>, Vec<u8>> {
    let mut result_img_buf =
        image::ImageBuffer::new(frames_count as u32 * frame_width, frame_height);

    // Extract frames data to get pixels buffer and a calculated position
    // for each pixel in the final output image buffer.
    let mut flatten_frames_buffers = RgbaFramesBuffer { frames: vec![] };

    let mut f_index: u32 = 0;
    for frame in raw_frames {
        let mut pix_index: u32 = 1; // Calculation is easier if pixels are indexed from 1 to n and not from 0.
        for p in frame.chunks(4).into_iter() {
            // Iterate over frame.buffer by chunks of 4 numbers, which are rgba values.
            let r = p[0];
            let g = p[1];
            let b = p[2];
            let a = p[3];

            // Calculate the local x, y position of the pixel in the frame
            let local_frame_px = {
                if pix_index % frame_width == 0 {
                    frame_width
                } else {
                    pix_index % frame_width
                }
            };
            let local_frame_py = ((pix_index - local_frame_px) / frame_width) + 1;

            // And calculate the pixel position in the output file deduced from
            // local position and the index of the frame
            let dest_px = local_frame_px + (f_index * frame_width) - 1;
            let dest_py = local_frame_py - 1;

            flatten_frames_buffers.frames.push(RgbaPix {
                color: vec![r, g, b, a],
                position: vec![dest_px, dest_py],
            });

            pix_index += 1;
        }
        f_index += 1;
    }

    // Iterate over the convenient data stored in flatten_frames_buffer
    // and copy each pixel at the specified position in output buffer.
    for source_pix in flatten_frames_buffers.frames {
        let r = source_pix.color[0];
        let g = source_pix.color[1];
        let b = source_pix.color[2];
        let a = source_pix.color[3];
        let x = source_pix.position[0];
        let y = source_pix.position[1];
        result_img_buf.put_pixel(x, y, image::Rgba([r, g, b, a]));
    }

    result_img_buf
}
