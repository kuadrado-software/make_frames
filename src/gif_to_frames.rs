//! The gif_to_frames modules handles a gif file as an input and buildss a single png file with all frames next to each other

use crate::frames::build_png_output;
use gif;
use std::fs::File;
use std::path::PathBuf;

/// Read input file, format frame data and output all frame to a single png file
pub fn build(path: &PathBuf, output_path: &PathBuf) {
    let mut decoder = gif::DecodeOptions::new();
    decoder.set_color_output(gif::ColorOutput::RGBA);

    let file = File::open(path).unwrap();
    let mut decoder = decoder.read_info(file).unwrap();

    let width = decoder.width() as u32;
    let height = decoder.height() as u32;

    // Copy gif frames into an simple Vec
    let mut frames = vec![];
    while let Some(f) = decoder.read_next_frame().unwrap() {
        frames.push(f.clone().buffer.into_owned());
    }
    let frames_count = frames.len();

    let result_img_buf = build_png_output(frames, width, height, frames_count);

    // Create the output file
    result_img_buf.save(output_path).unwrap();

    println!(
        "{}",
        String::from("Animation frames exported to file at ") + output_path.to_str().unwrap()
    );
}
