use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub struct CliArgs {
    pub mode: String,
    #[structopt(parse(from_os_str))]
    pub input_file: PathBuf,
    #[structopt(parse(from_os_str), short = "o", long = "output")]
    pub output_file: Option<PathBuf>,
}
